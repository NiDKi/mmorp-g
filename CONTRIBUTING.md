# Cloning the repo

Before starting to work on the repo, make sure you fork the repository. This
will allow you to have your own personal copy of the repo.

Then, clone your fork of the repository. This is where you'll be doing the
majority of your work. 

# Committing / Pushing Flow

Set the `upstream` remote to be the original `mmorp_g_` repository (This only
needs to be done once): 

```
$ git remote add upstream git@gitlab.com:mmorp-g/mmorp-g.git

$ git remote -v
origin	git@gitlab.com:<username>/mmorp-g.git (fetch)
origin	git@gitlab.com:<username>/mmorp-g.git (push)
upstream	git@gitlab.com:mmorp-g/mmorp-g.git (fetch)
upstream	git@gitlab.com:mmorp-g/mmorp-g.git (push)
```

Before creating a new branch, ensure your fork's main branch is up-to-date with
the upstream main branch: 
```
$ git checkout main
$ git pull upstream main
```

Now, create a branch, `feature-name`, edit some files, and make a commit: 

```
$ git checkout -b feature-name
$ # edit some files
$ git add <files-changed> 
$ git commit -m "Added a feature!"
```

Push to your (forked) repository: 

```
$ git push -u origin HEAD
```

This will create the appropriate branch in your forked repo. Now, create a merge
request via the online interface, and await for approval before merging. 
