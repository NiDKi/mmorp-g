#ifndef MMORPG_EFFECTS_H
#define MMORPG_EFFECTS_H

#include "Entity.h"

enum EffectTarget {              // How entities targets for the effect will be gotten
    EFFECT_TARGET_SELF,
    EFFECT_TARGET_TARGET,       // The source of the effect's target
    EFFECT_TARGET_RADIUS,       // All entities within a set radius of the source
    EFFECT_TARGET_AREA,         // All entities within a specified circular area
};

enum EffectGroup {               // what Entity will be affected based on combat status with the player
    EFFECT_GROUP_HOSTILE  = 0x01,
    EFFECT_GROUP_FRIENDLY = 0x02,
    EFFECT_GROUP_NEUTRAL  = 0x04,
    EFFECT_GROUP_ALL      = 0xff,
};

enum EffectType {                // how will the effect be applied
    EFFECT_TYPE_NORMAL,
    EFFECT_TYPE_BUFF,
};

enum EffectOut {                 // the outcome of the effect (the function to call)
    EFFECT_OUT_REGEN,
    EFFECT_OUT_DAMAGE,
};

struct Effect {

    EffectTarget target;
    uint16_t radius;
    uint16_t diameter;          //only used on area effects

    EffectGroup group;

    EffectType type;
    uint32_t duration;          // only used on buffs;
    uint32_t ticksToTrigger;    // how many ticks to trigger the buff effect, 0 for constant -only used on buffs

    EffectOut outcome;

    int32_t value;                  // value of the effect to be applied
};



// namespaced for encapsulation reasons
namespace effect {

    // find every target and call the corresponding effect function on them
    void process(Entity source, Effect effect, Entity target);

    //these functions are called on every target found according to the EffectTarget type
    void regen(Entity source, int value, Entity target);
    void damage(Entity source, int value, Entity target);

};

#endif          //MMORPG_EFFECTS_H
