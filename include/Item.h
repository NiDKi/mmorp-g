
#ifndef MMORPG_ITEM_H
#define MMORPG_ITEM_H

#include "Effects.h"

enum ItemType{
    ITEM_TYPE_EQUIPPABLE,
    ITEM_TYPE_EQUIPPABLE_PROCS,         //equips with on proc effects
    ITEM_TYPE_ON_USE,
};


struct Item {
    uint32_t id;
    uint32_t tex;                       //TODO: add texture enums

    ItemType itemType;
    Effect* effects;
};

#endif          //MMORPG_ITEM_H
