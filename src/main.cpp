/*******************************************************************************************
*
*	raylib [core] example - Basic window
*
*	Welcome to raylib!
*
*	To test examples, just press F6 and execute raylib_compile_execute script
*	Note that compiled executable is placed in the same folder as .c file
*
*	You can find all basic examples on C:\raylib\raylib\examples folder or
*	raylib official webpage: www.raylib.com
*
*	Enjoy using raylib. :)
*
*	This example has been created using raylib 1.0 (www.raylib.com)
*	raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*	Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include <string.h>
// FIXME this namespace is a little excessive atm
namespace raylib {
	#include "raylib.h"
}

void rlibCameraInit(raylib::Camera *rlibCamera)
{
	rlibCamera->position = (raylib::Vector3) { 18.83f, 12.89f, 12.0f };  	// approximations for trimetric (i think)
	rlibCamera->target = (raylib::Vector3) { 0.0f, 0.0f, 0.0f };	   	// Camera looking at point
	rlibCamera->up = (raylib::Vector3) { 0.0f, 1.0f, 0.0f };		// Camera up vector (rotation towards target)
	rlibCamera->fovy = 45.0f;						// Camera field-of-view Y
	rlibCamera->projection = raylib::CAMERA_PERSPECTIVE;			// Camera mode type

	SetCameraMode(*rlibCamera, raylib::CAMERA_CUSTOM);
}

void processInput(raylib::Vector3 *cubePosition, raylib::Camera *rlibCamera)
{
	if (raylib::IsKeyPressed(raylib::KEY_W)) {
		cubePosition->x -= 1.0;
		rlibCamera->position.x -= 1.0;
		rlibCamera->target.x -= 1.0;
	} else if (raylib::IsKeyPressed(raylib::KEY_S)) {
		cubePosition->x += 1.0;
		rlibCamera->position.x += 1.0;
		rlibCamera->target.x += 1.0;
	}
	if (raylib::IsKeyPressed(raylib::KEY_D)) {
		cubePosition->z -= 1.0;
		rlibCamera->position.z -= 1.0;
		rlibCamera->target.z -= 1.0;
	} else if (raylib::IsKeyPressed(raylib::KEY_A)) {
		cubePosition->z += 1.0;
		rlibCamera->position.z += 1.0;
		rlibCamera->target.z += 1.0;
	}
}

int main(void)
{
	// Initialization
	//--------------------------------------------------------------------------------------
	const int screenWidth = 800;
	const int screenHeight = 450;

	raylib::InitWindow(screenWidth, screenHeight, "mmorp/g/ WIP demo");
	// TODO add compile option for log levels
	raylib::SetTraceLogLevel(raylib::LOG_DEBUG);

	raylib::SetTargetFPS(60);		// Set our game to run at 60 frames-per-second

	raylib::Camera rlibCamera = { 0 };
	rlibCameraInit(&rlibCamera);

	raylib::Vector3 cubePosition = { 0.0f, 1.0f, 0.0f };
	raylib::Vector3 cube_size = { 2.0f, 2.0f, 2.0f };

	raylib::Vector3 prevCubePos = { 0 };
	memcpy(&prevCubePos, &cubePosition, sizeof(raylib::Vector3));
	//--------------------------------------------------------------------------------------

	// Main game loop
	while (!raylib::WindowShouldClose())	// Detect window close button or ESC key
	{
		// Update
		//----------------------------------------------------------------------------------
		processInput(&cubePosition, &rlibCamera);
		raylib::UpdateCamera(&rlibCamera);
		//----------------------------------------------------------------------------------

		// Draw
		//----------------------------------------------------------------------------------
		raylib::BeginDrawing();
		{
			raylib::ClearBackground(raylib::RAYWHITE);

			raylib::BeginMode3D(rlibCamera);
			{

				raylib::DrawCube(cubePosition, cube_size.x, cube_size.y, cube_size.z, raylib::RED);

				raylib::DrawGrid(50, 1.0f);
			}
			raylib::EndMode3D();

			//DrawText("Congrats! You created your first window!", 190, 200, 20, LIGHTGRAY);
		}
		raylib::EndDrawing();

		// TODO: this is for demo, remove this
		if (memcmp(&prevCubePos, &cubePosition, sizeof(raylib::Vector3)) != 0) {
			raylib::TraceLog(raylib::LOG_DEBUG, "CUBE: %.1f, %.1f, %.1f", cubePosition.x, cubePosition.y, cubePosition.z);
			memcpy(&prevCubePos, &cubePosition, sizeof(raylib::Vector3));
		}
		//----------------------------------------------------------------------------------
	}

	// De-Initialization
	//--------------------------------------------------------------------------------------
	raylib::CloseWindow();		  // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}
